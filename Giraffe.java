public class Giraffe{
	public double height;
	public double speed;
	public int lifespan;
	public boolean canFight;
	
	public void eatTrees(double x){
		System.out.println("This Giraffe can eat trees that are " + x +" meters tall");
	}
	
	public void fightContender(boolean a){
		if (a == true){
			System.out.println("This Giraffe can fight!");
		}
		else {
			System.out.println("This Giraffe is a weakling and cannot fight for their life!");
		}
		
	}
	
}