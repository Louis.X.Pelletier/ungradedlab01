public class Application{
	
	public static void main(String[] args){
		Giraffe alphamale = new Giraffe();
		Giraffe betamale = new Giraffe();
		
		alphamale.height = 5.5;
		alphamale.speed = 60;
		alphamale.lifespan = 25;
		alphamale.canFight = true;
		
		betamale.height = 4.3;
		betamale.speed = 30;
		betamale.lifespan = 15;
		betamale.canFight = false;
		
		System.out.println(alphamale.height);
		System.out.println(alphamale.speed);
		System.out.println(alphamale.lifespan);
		System.out.println(alphamale.canFight);
		System.out.println(betamale.height);
		System.out.println(betamale.speed);
		System.out.println(betamale.lifespan);
		System.out.println(betamale.canFight);
		
		alphamale.eatTrees(alphamale.height);
		betamale.eatTrees(betamale.height);
		alphamale.fightContender(alphamale.canFight);
		betamale.fightContender(betamale.canFight);
		
		Giraffe[] herd = new Giraffe[3];
		herd[0]= alphamale;
		herd[1]= betamale;
		herd[2]= new Giraffe();
	}

}